
<h1 style="color:#0F1992;text-align:center;"> Tabla de Control de Documentos</h1>
<table id="" class="" style="color: black;margin-top: 20px; font-size: 12px" >
    <thead id="header" class="">
    <tr style="">
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Gerencia</th>
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Área</th>   
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Indicador</th>
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Titulo</th>
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Tipo de documento</th>      
        <th style="border: solid 1px grey;color:#EBEBEC;background-color:#232323;">Estado</th>
        
    </tr>
    </thead>
    <tbody>
    @foreach($documentos as $doc)
        <tr>
            <td>{{$doc->gerencia}}</td>
            <td>{{$doc->area}}</td>
            <td>{{$doc->codIndicador}}</td>
            <td>{{$doc->titulo}}</td>
            <td>{{$doc->nombreDoc}}</td>
            @if($doc->estado == 1)
            <td style="color:white;background-color:green">Desarrollado</td>
            @else
            <td style="color:white;background-color:#F19696;">Faltante</td>
            @endif
            
        </tr>
    @endforeach

    </tbody>

</table>

